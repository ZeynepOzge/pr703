package com.example.p703;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static com.example.p703.P703Application.calculate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
class P703ApplicationTests {

    @Test
    public void testAddPositiveNumbers() {

        assertEquals(calculate(new int[] {5, 5,10}), 20);
    }


    @Test
    public void testAddPositiveandNegativeNumbers() {

        assertEquals(calculate(new int[] {-5, 5,10}), 10);
    }


    @Test
    public void testAddZero() {

        assertEquals(calculate(new int[] {0}), 0);
    }

    @Test
    public void testAddNegativeNumbers() {

        assertEquals(calculate(new int[] {-1,-3,-7}), -11);
    }


    @Test
    void test_RealFail() {
        assertNotEquals(calculate(new int[] {5, 5,10}),10);
    }

   @Test
    void test_Fail() {
        assertNotEquals(calculate(new int[] {5, 5,10}),20);
    }


}
